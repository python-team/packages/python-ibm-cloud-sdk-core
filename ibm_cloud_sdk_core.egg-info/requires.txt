requests<3.0.0,>=2.31.0
urllib3<3.0.0,>=2.1.0
python_dateutil<3.0.0,>=2.8.2
PyJWT<3.0.0,>=2.8.0
